install:
	poetry lock --no-update
	poetry install

build: install
	poetry run pip install pre-commit
	poetry run pre-commit install

unit-test: install
	poetry run pytest tests/unit

integration-test: install
	docker-compose -f docker/docker-compose.databases.yaml up -d
	# Waiting for Postgres database to start up
	sleep 3
	poetry run pytest tests/integration
	docker-compose -f docker/docker-compose.databases.yaml down
