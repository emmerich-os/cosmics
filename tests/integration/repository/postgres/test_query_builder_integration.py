import pytest

from cosmics.repository.postgresql import _query_builder as query_builder


@pytest.mark.parametrize(
    "table, return_output, data, expected",
    [
        (
            "test_table",
            False,
            {"test_column_1": "test_value_1"},
            (
                """INSERT INTO "test_table" """
                """("test_column_1") """
                """VALUES """
                """('test_value_1');"""
            ),
        ),
        (
            "test_table",
            True,
            {"test_column_1": "test_value_1"},
            (
                """INSERT INTO "test_table" """
                """("test_column_1") """
                """VALUES """
                """('test_value_1') """
                """RETURNING *;"""
            ),
        ),
        (
            "test_table; INJECTION;",
            False,
            {"test_column_1; INJECTION;": "test_value_1; INJECTION;"},
            (
                """INSERT INTO "test_table; INJECTION;" """
                """("test_column_1; INJECTION;") """
                """VALUES """
                """('test_value_1; INJECTION;');"""
            ),
        ),
    ],
)
def test_create_insert_query(db_connection, table, return_output, data, expected):
    query = query_builder.create_insert_query(
        table, return_output=return_output, data=data
    )

    result = query.as_string(db_connection)

    assert result == expected


@pytest.mark.parametrize(
    "table, expected",
    [
        (
            "test_table",
            'SELECT * FROM "test_table" ',
        ),
        (
            "test_table; INJECTION;",
            'SELECT * FROM "test_table; INJECTION;" ',
        ),
    ],
)
def test_create_select_query(db_connection, table, expected):
    query = query_builder.create_select_query(table)

    result = query.as_string(db_connection)

    assert result == expected


@pytest.mark.parametrize(
    "table, values, expected",
    [
        (
            "test_table",
            {"test_column": 1},
            """UPDATE "test_table" SET  "test_column" = 1 """,
        ),
        (
            "test_table; INJECTION;",
            {"test_column; INJECTION;": "test_value_1; INJECTION;"},
            """UPDATE "test_table; INJECTION;" SET  "test_column; INJECTION;" = 'test_value_1; INJECTION;' """,
        ),
    ],
)
def test_create_update_query(db_connection, table, values, expected):
    query = query_builder.create_update_query(
        table=table,
        data=values,
    )

    result = query.as_string(db_connection)

    assert result == expected


@pytest.mark.parametrize(
    "data, expected",
    [
        (
            {"test_column": "test_value_1"},
            """ WHERE  "test_column" = 'test_value_1' """,
        ),
        (
            {"test_column; INJECTION;": "test_value_1; INJECTION;"},
            """ WHERE  "test_column; INJECTION;" = 'test_value_1; INJECTION;' """,
        ),
    ],
)
def test_create_where_clause(db_connection, data, expected):
    clause = query_builder.create_where_clause(data)

    result = clause.as_string(db_connection)

    assert result == expected
