import pytest


class TestPostgresqlClient:
    @pytest.mark.parametrize(
        ("table", "data", "expected"),
        [
            (
                "test_table",
                {"test_column": "test_value"},
                {"test_column": "test_value"},
            ),
            (
                "test_table",
                {"test_column": "test_value", "test_non_existing_column": None},
                {"test_column": "test_value"},
            ),
        ],
    )
    @pytest.mark.usefixtures("_clean_db")
    def test_insert(self, client, table, data, expected):
        client.insert(target=table, data=data)

        [result] = client.select(target=table, where=data)

        assert result == expected

    @pytest.mark.parametrize(
        ("table", "data", "expected"),
        [
            ("test_table", {"test_column": "test_value"}, 0),
            (
                "test_table",
                {"test_column": "test_value", "test_non_existing_column": None},
                0,
            ),
        ],
    )
    @pytest.mark.usefixtures("_clean_db")
    def test_insert_and_return_row_id(self, client, table, data, expected):
        result = client.insert_and_return_row_id(target=table, data=data)

        assert result == expected

    @pytest.mark.parametrize(
        ("table", "where"),
        [
            ("test_table", {"test_column": "test_value"}),
            (
                "test_table",
                {"test_column": "test_value", "test_non_existing_column": None},
            ),
        ],
    )
    @pytest.mark.usefixtures("_db_with_data")
    def test_select(self, client, table, where):
        assert client.select(target=table, where=where)

    @pytest.mark.parametrize(
        ("table", "data", "where", "expected"),
        [
            (
                "test_table",
                {"test_column": "test_value_changed"},
                {"test_column": "test_value"},
                {"test_column": "test_value_changed"},
            ),
            (
                "test_table",
                {"test_column": "test_value_changed", "test_non_existing_column": None},
                {"test_column": "test_value", "test_non_existing_column": None},
                {"test_column": "test_value_changed"},
            ),
        ],
    )
    @pytest.mark.usefixtures("_db_with_data")
    def test_update(
        self,
        client,
        table,
        data,
        where,
        expected,
    ):
        client.update(target=table, data=data, where=where)

        [result] = client.select(target=table, where=data)

        assert all(result[key] == value for key, value in expected.items())

    @pytest.mark.parametrize(
        ("table", "where"),
        [
            ("test_table", {"test_column": "test_value"}),
            (
                "test_table",
                {"test_column": "test_value", "test_non_existing_column": None},
            ),
        ],
    )
    @pytest.mark.usefixtures("_db_with_data")
    def test_delete(self, client, table, where):
        client.delete(target=table, where=where)

        result = client.select(target=table, where=where)

        assert not result
