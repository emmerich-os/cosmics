import os
from typing import Any
from typing import Union

import psycopg2
import pytest

from cosmics import postgresql


@pytest.fixture(scope="session")
def db_info() -> dict[str, Union[int, str]]:
    """Read test DB information from environment variables."""
    return {
        "database": os.getenv("DB_NAME", default="test_database"),
        "user": os.getenv("DB_USER", default="test_user"),
        "password": os.getenv("DB_USER_PASSWORD", default="test_password"),
        "host": os.getenv("DB_HOST", default="127.0.0.1"),
        "port": int(os.getenv("DB_PORT", default=5432)),
    }


@pytest.fixture(scope="session")
def db_connection(db_info):
    """Return connection to a local PostgreSQL database.

    Used for encoding 'sql.Composed`to `str`.

    """
    return psycopg2.connect(**db_info)


@pytest.fixture(scope="session")
def create_tables_queries(tables) -> list[str]:
    return ["CREATE TABLE IF NOT EXISTS test_table (test_column TEXT);"]


@pytest.fixture(scope="session")
def tables() -> list[str]:
    """Return all database objects created with the migration scripts in `docker/migrations/`."""
    return ["test_table"]


@pytest.fixture(scope="session")
def delete_queries(tables) -> list[str]:
    """Return a delete query to clear all tables defined in the `objects` fixture."""
    return [f"DELETE FROM {table};" for table in tables]


@pytest.fixture(scope="session")
def _db_instance(db_info, create_tables_queries, delete_queries):
    client = postgresql.Client(**db_info)
    _exec_queries(client, create_tables_queries)
    _exec_queries(client, delete_queries)
    yield
    _exec_queries(client, delete_queries)


@pytest.fixture(scope="session")
def client(_db_instance, db_info) -> postgresql.Client:
    """Return `cod4pugbot.db.SQLClient` with a connection to the DB.

    When called, all tables in the DB will be cleared.

    """
    return postgresql.Client(**db_info)


@pytest.fixture()
def _clean_db(client, delete_queries) -> None:
    _exec_queries(client, delete_queries)


@pytest.fixture()
def rows() -> dict[str, list[dict[str, Any]]]:
    return {"test_table": [{"test_column": "test_value"}]}


@pytest.fixture()
def _db_with_data(_clean_db, client, rows) -> None:
    for target, row_data in rows.items():
        for data in row_data:
            client.insert(target=target, data=data)


def _exec_queries(client: postgresql.Client, queries: list[str]) -> None:
    """Execute an SQL query."""
    with client.connection as connection:
        with connection.cursor() as cursor:
            for subquery in queries:
                cursor.execute(subquery)
        connection.commit()
