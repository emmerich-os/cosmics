import dataclasses

import pytest

from cosmics import domain
from cosmics import testing
from cosmics.service_layer import messagebus
from cosmics.service_layer import unit_of_work


@pytest.fixture()
def fake_uow() -> testing.FakeUnitOfWork:
    return testing.FakeUnitOfWork()


@pytest.fixture()
def fake_database_uow(client_with_models) -> testing.FakeDatabaseUnitOfWork:
    return testing.FakeDatabaseUnitOfWork(client_with_models)


@dataclasses.dataclass(frozen=True)
class Command(domain.Command):
    caused_events: list[domain.Event] = dataclasses.field(default_factory=list)

    def caused_event(self, event: domain.Event) -> None:
        self.caused_events.append(event)


@dataclasses.dataclass(frozen=True)
class CommandWithoutHandler(Command):
    pass


@dataclasses.dataclass(frozen=True)
class AsyncCommand(Command):
    pass


@dataclasses.dataclass(frozen=True)
class CommandWithoutDatabase(Command):
    pass


@dataclasses.dataclass(frozen=True)
class CommandWithDatabase(domain.Command):
    item: domain.AbstractModel

    caused_events: list[domain.Event] = dataclasses.field(default_factory=list)

    def caused_event(self, event: domain.Event) -> None:
        self.caused_events.append(event)


@dataclasses.dataclass(frozen=True)
class CommandWithAdditionalDependency(Command):
    pass


@dataclasses.dataclass(frozen=True)
class CommandWithExceptionRaised(Command):
    pass


class AnyException(Exception):
    """Any exception raised."""


@dataclasses.dataclass(frozen=True)
class EventWithoutHandler(domain.Event):
    pass


@dataclasses.dataclass(frozen=True)
class Event(domain.Event):
    _executed: list = dataclasses.field(default_factory=list)

    def execute(self) -> None:
        self._executed.append(None)

    @property
    def executed(self) -> bool:
        if self._executed:
            return True
        return False


@dataclasses.dataclass(frozen=True)
class EventWithExceptionRaised(domain.Event):
    pass


def do_something(command: CommandWithoutDatabase, uow: testing.FakeUnitOfWork) -> None:
    event = Event()
    command.caused_event(event)
    uow.add_event(event)


async def do_something_async(
    command: AsyncCommand, uow: testing.FakeUnitOfWork
) -> None:
    event = Event()
    command.caused_event(event)
    uow.add_event(event)


def raise_exception_for_command(
    command: CommandWithExceptionRaised, uow: testing.FakeUnitOfWork
) -> None:
    raise AnyException("An error occurred while handling a command")


def do_something_with_additional_dependency(
    command: CommandWithAdditionalDependency,
    uow: testing.FakeUnitOfWork,
    dependency: int,
):
    event = Event()
    command.caused_event(event)
    uow.add_event(event)


def add_model_to_repository(
    command: CommandWithDatabase, uow: testing.FakeDatabaseUnitOfWork
) -> None:
    with uow:
        uow.repository.add(command.item)
    event = Event()
    command.caused_event(event)
    uow.add_event(event)


def react_to_event(event: Event):
    event.execute()


def raise_exception_for_event(event: EventWithExceptionRaised):
    raise AnyException("An error occurred while handling an event")


COMMAND_HANDLERS = {
    CommandWithoutDatabase: do_something,
    CommandWithDatabase: add_model_to_repository,
    CommandWithExceptionRaised: raise_exception_for_command,
    AsyncCommand: do_something_async,
    CommandWithAdditionalDependency: do_something_with_additional_dependency,
}

EVENT_HANDLERS = {
    Event: [react_to_event],
    EventWithExceptionRaised: [raise_exception_for_event],
}


@pytest.fixture()
def messagebus_without_database(fake_uow):
    return _create_messagebus(fake_uow)


@pytest.fixture()
def messagebus_with_database(fake_database_uow):
    return _create_messagebus(fake_database_uow)


def _create_messagebus(uow: unit_of_work.AbstractUnitOfWork) -> messagebus.MessageBus:
    return messagebus.MessageBus(
        uow=uow, command_handlers=COMMAND_HANDLERS, event_handlers=EVENT_HANDLERS
    )


@pytest.mark.asyncio()
async def test_handle_command_without_handler_defined(messagebus_without_database):
    command = CommandWithoutHandler()

    with pytest.raises(messagebus.MissingHandlerException):
        await messagebus_without_database.handle(command)


@pytest.mark.asyncio()
async def test_handle_event_without_handler_defined(messagebus_without_database):
    event = EventWithoutHandler()

    with pytest.raises(messagebus.MissingHandlerException):
        await messagebus_without_database.handle(event)


@pytest.mark.asyncio()
async def test_handle_without_database(messagebus_without_database):
    command = CommandWithoutDatabase()

    await messagebus_without_database.handle(command)
    [event] = command.caused_events

    assert event.executed


@pytest.mark.asyncio()
async def test_handle_async_command(messagebus_without_database):
    command = AsyncCommand()

    await messagebus_without_database.handle(command)
    [event] = command.caused_events

    assert event.executed


@pytest.mark.asyncio()
async def test_handle_with_database(messagebus_with_database):
    command = CommandWithDatabase(item=testing.FakeModel(id=1))

    await messagebus_with_database.handle(command)
    [event] = command.caused_events

    assert event.executed


@pytest.mark.asyncio()
async def test_raise_of_exception_when_handling_command(messagebus_without_database):
    command = CommandWithExceptionRaised()
    with pytest.raises(AnyException):
        await messagebus_without_database.handle(command)


@pytest.mark.asyncio()
async def test_handle_command_with_additional_dependency(messagebus_without_database):
    dependency = 1
    command = CommandWithAdditionalDependency()

    await messagebus_without_database.handle(command, dependency=dependency)
    [event] = command.caused_events

    assert event.executed


@pytest.mark.asyncio()
async def test_raise_of_exception_when_handling_event(messagebus_without_database):
    event = EventWithExceptionRaised()
    # Handling events is allowed to fail silently (with the exception logged)
    await messagebus_without_database.handle(event)
