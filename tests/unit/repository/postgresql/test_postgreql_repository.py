import pytest

from cosmics import testing
from cosmics.repository import postgresql


@pytest.fixture()
def postgresql_repository(all_models_as_dicts) -> postgresql.Repository:
    client = testing.FakePostgresqlClient(contains=all_models_as_dicts)
    return testing.FakePostgresqlRepository(client)


class TestAbstractRepository:
    def test_insert_and_return_row_id(self, postgresql_repository, any_model):
        # Expected ID is in the FakePostgresqlClient's method
        expected = 1

        result = postgresql_repository.add_and_return_row_id(any_model)

        assert result == expected
