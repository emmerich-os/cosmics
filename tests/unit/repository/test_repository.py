import pytest

from cosmics import repository
from cosmics import testing


class TestAbstractRepository:
    def test_items(self, any_repository, all_models):
        result = any_repository.items

        assert result == all_models

    def test_add(self, any_repository):
        item = testing.FakeModel(id=1)
        any_repository.add(item)

        assert item in any_repository.added

    def test_get(self, any_repository, any_model):
        result = any_repository.get(any_model.identifier)

        assert result == any_model

    def test_get_missing(self, any_repository):
        missing = testing.FakeModel(id=9999)

        with pytest.raises(repository.NotFoundInDatabaseException):
            any_repository.get(missing.identifier)

    def test_update(self, any_repository, any_model):
        any_model.test_key = "test_value_changed"

        any_repository.update(any_model)
        result = any_repository.get(any_model.identifier)

        assert result == any_model

    def test_delete(self, any_repository, all_models):
        for model in all_models:
            any_repository.delete(model)

        assert not any_repository.items
