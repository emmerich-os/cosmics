import pytest

from cosmics import repository
from cosmics import testing


@pytest.fixture()
def any_model() -> testing.FakeModel:
    return testing.FakeModel(id=1)


@pytest.fixture()
def another_model() -> testing.FakeModel:
    return testing.FakeModel(id=2)


@pytest.fixture()
def all_models(any_model, another_model) -> list[testing.FakeModel]:
    return [any_model, another_model]


@pytest.fixture()
def all_models_as_dicts(all_models) -> list[dict]:
    return [model.to_dict() for model in all_models]


@pytest.fixture()
def client_with_models(all_models_as_dicts) -> repository.AbstractClient:
    return testing.FakeClient(contains={"test_target": all_models_as_dicts})


@pytest.fixture()
def any_repository(client_with_models) -> repository.AbstractRepository:
    return testing.FakeRepository(client_with_models)
