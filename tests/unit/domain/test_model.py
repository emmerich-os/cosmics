import pytest

from cosmics import testing


class TestAbstractModel:
    @pytest.mark.parametrize(
        ("other", "expected"),
        [
            (testing.FakeModel(id=1), True),
            (testing.FakeModel(id=2), False),
            ("a string", False),
        ],
    )
    def test_eq(self, any_model, other, expected):
        result = any_model == other
        assert result == expected

    def test_hash(self, any_model):
        # An object must be hashable to be used in set
        s = {any_model}
        assert s

    def test_update(self, any_model):
        data = {
            "test_key": "test_value_changed",
            "test_key_invalid": "should be filtered",
        }
        expected = testing.FakeModel(id=any_model.id, test_key="test_value_changed")

        result = any_model.update(data)

        assert result == expected
        assert result.test_key == expected.test_key

    def test_to_dict(self, any_model):
        expected = {"id": 1, "test_key": "test_value"}

        result = any_model.to_dict()

        assert result == expected
