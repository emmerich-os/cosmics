.. cosmics documentation master file, created by
   sphinx-quickstart on Wed Jul  7 12:44:55 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


cosmics - Documentation
================================

.. toctree::
   :maxdepth: 2

   cosmics
   modules
